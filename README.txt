README.txt
____________________

Module Name: Privatemsg Translator
Drupal Project Page: (coming soon)
Current Version: 7.x-1.x
Drupal Core: 7.x
Revision update: December 15th, 2014


DESCRIPTION
____________________

Chat in real-time or send messages to other users with automatic language
translation.

Privatemsg Translator will help users with different default languages to
communicate through automatic message translation via the amazing Privatemsg
module and Google Translate API integration. When a user sends a message through
the Privatemsg module, the Privatemsg Translator module will run through this
process:

1. Check if the users have different languages set.

2. If different languages are discovered, send the message to Google Translate.

3. Retrieve the translated message from Google Translate.

4. Display the translated message with the original message.

Currently, the module is ready for alpha release as the module works as
described above. For real-time chat tanslation, this module has been tested and
works with the Private message with node.js module.


FUTURE ENHANCEMENTS
____________________ 

Actively developing future enhancements including the ability to separate the
original message and the translated message into better mark-up elements for
theming.


INSTALLATION
____________________

To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the privatemsg directory and all its contents to your modules
directory.

3. Visit admin/modules and enable the "Privatemsg Translator" module.


CONFIGURATION
____________________

To configure this module do the following:

1. Obtain a Google Translate API key (see 3rd PARTY API section below for more
information).

2. Go to Configuration -> Privatemsg Translator -> Privatemsg Translator
Settings (admin/config/privatemsgtranslator/settings).

3. Check the box for "Enable Google Translate API".

4. In the Google Translate API key box, enter your API key that you should have
already retrieved from Google.

5. Save the settings.


DEMO SITE
____________________

 * Privatemsg Translator module homepage:
 * http://kennethlancaster.com/translator (coming soon)
 
 * Privatemsg Translator module Drupal 7 demonstration website:
 * http://translator.kennethlancaster.com/ (coming soon)


DEVELOPERS
____________________
 
 * Developed by Kenneth Lancaster for contribution to the Drupal community:
 * http://kennethlancaster.com (coming soon)
 
 * Development contribution/help by Rick Tilley:
 * http://pcrats.com/


REQUIRED MODULES
____________________
 
 * Privatemsg Translator module is meant to be used with the Privatemsg module:
 * https://www.drupal.org/project/privatemsg
 
 * Required modules may have their own dependencies.


OPTIONAL MODULES
____________________
 
 * For real-time chat translation you will need to use Private message with
   node.js:
 * https://www.drupal.org/project/privatemsg_nodejs
 
 * Optional modules may have their own dependencies.


3rd PARTY API
____________________
 
 * Requires a Google Translate api key:
 * https://cloud.google.com/translate/
 
 * Google Translate API is not free, be sure to read the details on the Google
 Translate API's pricing website:
 * https://cloud.google.com/translate/v2/pricing

 
TERMS OF MODULE USE
____________________ 

By using the Privatemsg Translator module you are agreeing that you will not
hold the developers of this module, related modules, or Drupal, liable or
responsible for any charges that may incur through use of the Google Translate
API. Again, be sure to read the fine print for Google Translate API and be sure
to note that you can utilize various settings on the Privatemsg module that can
help limit the number of messages sent/received if needed.


DISCLAIMER
____________________ 

The developers of the Privatemsg Translator module are not affiliated with
Google in any way. Nor do the developers receive any compensation from Google
for your use of this module. Although, it would be nice if Google did give us
a little kick-back;) We hope you enjoy use of this module.
